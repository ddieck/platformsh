<html>
<head>
	<title>Test</title>
</head>
<body>
<?php
$server   = "database.internal";
$user     = "admin";
$password = "";
$db 	  = 'main';

try {
	$conn = new PDO("mysql:host=$server;dbname=$db", $user, $password);
	$username = $_GET['user'];
	$entry = $_GET['entry'];
	$sql = "CREATE TABLE IF NOT EXISTS Entry (
		id INT (6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		user VARCHAR(8),
		entry VARCHAR(1000)
	);";
	$conn->exec($sql);

	$sql = $conn->prepare("INSERT INTO Entry (user, entry) VALUES (?, ?);");
	$sql->execute(array($username,$entry));

	$stmt = $conn->prepare("SELECT id, user, entry from Entry");
	$stmt->execute();
	while ($row = $stmt->fetch()) {
		echo $row['id'] . " " . $row['user'] . " " . $row['entry'] . "<br />";
	}
	$conn = null;

} catch(PDOException $e) {
	echo "Connection failed: " . $e->getMessage();
}




?>
</body>
</html>
